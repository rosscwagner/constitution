Goal: 8000 words . About equal to the U.S constitution including all amendments. 
Current: ~1930.

Guiding values:

* Direct democracy. Economic and political.
* Decentralization of power. Concentrating power invites corruption, creates bottlenecks,  . 
* Instances where one has power over another's safety, health, livelihood, ect are unacceptable.
* Localization of power. Power must be located as close as possible to those it affects. Locals tend to know best. 
* Individual freedom. Restrictions on individual behavior must be limited.
* Transparency. Those in power must be closely scrutinized.
* Oversight. Mistakes and abuses must be dealt with immediately an justly.
* Recallability. All public servants elected, appointed, delegated, or randomly selected, can be recalled from their position, potentially replaced via election and punished for their wrong doings.
* usufruct, the irreducible minimum, and complementarity. [TODO: explain these]
* Restorative justice. When antisocial behavior occurs, first repair the damage done, next investigate why the anti social action was taken, finally take steps to mitigate reason antisocial action was taken.

## Abstract

The goal of these documents is to rework the American political system to be based around local community councils as the primary political unit. Communities federate to from regional, national, ect councils to resolve disputes, requisition resources, and facilitate cooperation. To have fewer points of failure for corruption, members of a community who have opted in, are randomly selected to be local council. Terms are short so the burden of leadership is shared.The goal is to remove professional politicians whose material interests diverge from those they represent. Councils must send a recallable delegate to the next highest council. Delegates act as a liaison making the needs of their constituents known and communicating operations of higher councils to lower ones. 

## Requirements

To maintain good standing, community councils must, to the best of their ability [are these weasel words bad actors can use to avoid responsibility?], fulfill a number of requirements. Failure to do so risks investigation from the higher councils, punishment for those responsible, as well as loss of access to mutual aid, trade, defense, travel, and communication networks.

1. The community must make available items needed for imitate survival. Food, water, shelter.

2. Universal healthcare. To the best of its ability, the community must ensure medical treatment to its citizens. Can be done through incentives for medical training and residency. 

1. Universal Suffrage. All adult residents and non violent felons who have served their sentence have the right to vote and are eligible for any random selection the community employs. No governing body may pass law restricting voting rights of its residents and must do everything in its power to make voting accessible. Waiting times for in person voting must be kept under an hour and voting by mail should be used where applicable. 

1. Freedom of speech. No governing body can can take negative action against citizens for something they have said. One can be punished for speech that incites violence. The classic "Yelling fire in a crowded theater" comes to mind and is punishable. Another example of unacceptable speech is hate speech. Speech advocating for violent ideologies such as Nazism and white supremacy are also punishable. To maintain a tolerant society, intolerance must be rejected. All other speech, whether it is criticizing the government or advocating change, is protected. [The guiding heuristic here is that speaking hatefully about someone for a trait they have no control over is unacceptable. Speaking hatefully against a person of color is unacceptable because people have no control over what race they are born as. where as speaking hatefully against landlords is less unacceptable because a landlord has unjustifiable power and can stop being a landlord whenever they want. The definition of unacceptable speech must be kept as narrow as possible to prevent abuse. We do not want the definition of unacceptable speech to balloon to whatever the party in power wants it to be] 

1. Freedom of press. [explain]

1. Freedom of religion. [explain]

1. Right to bear arms. Under no pretext should arms and ammunition be surrendered; any attempts to disarm the people must be stopped, by force if necessary. [Mass shootings are a thing though and we should do something about that]

1. Mutual defense. All communities must agree to non hostility towards all members of the federation as well as mutual defense for all communities in the federation. All communities must maintain a well regulated militia. Officers are democratically elected by the soldiers they command. In emergency situations militias can organize into a federal army, electing a temporary commander-in-chief. [define rules on foreign actions]

1. Ban quartering of soldiers. No Soldier shall, in time of peace be quartered in any house, without the consent of the Owner, nor in time of war, but in a manner to be prescribed by law.

1. Ban warrant-less search and seizure. The right of the people to be secure in their persons, houses, papers, and effects, against unreasonable searches and seizures, shall not be violated, and no Warrants shall issue, but upon probable cause, supported by Oath or affirmation, and particularly describing the place to be searched, and the persons or things to be seized.

1. Right to a trial by a jury of peers. 

1. Right to a speedy, public trial and legal defense. Failure to hold a trail when able will result in a pardon of the accused. []

1. Ban cruel and unusual punishment. Excessive bail shall not be required, nor excessive fines imposed, nor cruel and unusual punishments inflicted.

1. Powers not delegated to higher councils or banned from local councils belong to local councils. 

1. Guaranteed employment. Communities must pair willing, idle hands with needed tasks.

2. Ban forced labor. Positive incentives only. No prison slavery. 

2. Grantee access to education. Access to education must not be bared based on one's ability to pay. Mandatory grade 1 to 12. Optional higher education.

4. Ban plurality/first past the post voting systems. [Approval](https://en.wikipedia.org/wiki/Approval_voting), [ranked choice](https://en.wikipedia.org/wiki/Instant-runoff_voting), [STAR](https://en.wikipedia.org/wiki/STAR_voting), or other alternative voting methods must be used for all elections with more than 2 candidates. A healthy democracy requires an accurate [not the right word] voting system.

![https://electionscience.org/library/approval-voting/](comparing_voting_methods_simplicity_group_satisfaction-1.png)

5. Unrestricted trade between communities in federation. Councils at all levels must collaborate to facilitate trade, travel, and communication networks between communities.

## Council Framework Structure

All power in America derives from the people and belongs to the people. The people exercise power by organizing together to form communities and democratically select a council of representatives, which constitute the political foundation of America. Communities are not permitted to deny membership due to ability to pay, work, or on the basis of pre-existing conditions. America is structured around the iterative federation of local communities. Local communities may join together to form a regional community with a regional council. Regional communities may join together to form a national community with a national council. This process can be repeated such that communities' constituents feel adequately represented by councils of approximately 100 members[Dunbar's number].

Local councils have the power to transfer ownership of capital property such as factories and farms directly to a democratic union of employees of said capital property [how determine ownership/management? council appointment or direct worker?]. Local councils do not have the ability to lay claim to any personal property such as tooth brushes and personal vehicles. 

Any member of all councils can be [recalled](https://en.wikipedia.org/wiki/Recall_election) at anytime if their constituents decide. Furthermore, at the end of their term, previous council members can be tried for and convicted of any wrong doings by the incoming council representatives. 

Sole power to pass and enforce legislation will reside in local councils. Higher councils may pass recommendations but are required to respect the sovereignty of local councils. Decisions of local councils can be overridden by a super-majority in the next highest council. Local councils can appeal to the council above the overriding one to cancel the override. [what %? similar to supreme court. needs clarification]

Selection for various local councils must be provably random, pulling form a body of adult residents who have opted in and term limits must be short (less than 4 years).

Recommended group decision making system. 
![An example flowchart illustrating modified consensus](./ModifiedConsensusFlowchart.png)

[I'm not sure if dividing powers and responsibilities is desirable here. What is the justification for splitting powers like this? Does it apply here?]

### Legislative Council  

A council of approximately 100 community residents will have power to draft and enact legislation. Legislation may not violate any of the requirements listed in this constitution. Beyond that local communities, and by extension this local community legislative council, are most familiar with their needs and most knowledgeable on how to meet them and are therefore given primary sovereignty. 

### Judicial Council 

A council of approximately 100 community residents will have power to rule on constitutionality of laws and legislation passed by the Legislative Council. This Judicial Council also has the power to hire and fire judges as well as enact policy on juries.

#### Juries 

Juries must be randomly selected from the community. Juries have final say on the verdict, guilty or not guilty, of the defendant. Juries also have the final say on the punishment of the guilty defendant, with the guidance and recommendation of the judge.  The judge proposes a sentence and juries vote to enact that sentence. [What threshold for sentence enactment. 100%, 2/3, 50% ?]

#### Judges

Judges are legal experts who moderate the trial between the persecutor and defendant, provide advice and guidance to the jury, and provide sentence proposals for the jury to vote on.

### Executive Council

A council of approximately 100 community residents will have power to enforce laws and legislation passed by the Legislative Council. The Executive Council will also have power to hire and fire enforcement officers (police), determine the equipment and methods available to said officers, as well as determine policy on prisons and rehabilitation. 

[Focus in prisons should be on rehabilitation over punishment]

## Patenting and property 

[Goals: No patent trolls, no shutdowns, no cease and desist. However, also rewards creators]

Aim for [usufruct](https://en.wikipedia.org/wiki/Usufruct) property relations.

Intellectual property is a fuck. 

Rewards for innovation must be given to inventors by all levels of federation in exchange for public availability of invention? [on learning more I'm not as convinced that rewards are as necessary as I previously thought]

An individual or origination can not own the sole right to copy and distribute intellectual property. An individual's right to creation and expression supersedes any claim to sole ownership of profit and use. Instead, patent holders may claim a small percentage of windfall [or profit?]. 

## Money

Money is a fuck. Strive for gift economy. Accept compromise to labor vouchers if necessary. 

[Who controls the printing of money? Highest council? Letting local communities mint their own currency could lead to problems. Some think money can be factored out through some form of planning but this is not a hill I am willing to die on. Digital currency could be something to consider.]

## Distribution

Production and distribution for profit are not options. In no way should one be able to amass wealth simply by owning. 

- [Decentralized planning](https://en.wiktionary.org/wiki/decentralized_planning). In my mind decentralized planning is analogous to bartering on a community scale. Communities know what they need and what they can produce. Communities then come together in a sort of market place where communities trade what they have surplus of in exchange for what they have deficits of. Communities are then free to distribute resources to their residents however they want. Local distribution can be done through free shops, labor vouchers, prioritized lists, money, rationing, lottery, or any other way I am not clever or creative enough to come up with. 
	- pros: 
		- economic information preserved when compared to centralized planning
		- direct democratization of distribution possible 
		- money could be factored out
	- cons:
		- hard to explain and implement
		
- [Gift economy](https://en.wikipedia.org/wiki/Gift_economy):  The goal to strive for. Take steps to ensure folks get what they need so that they can give to the best of their ability. A gift economy can be thought of abstractly as a generalized potluck/library economy. 
	- pros:
		- market-less
		- no need for planning
		- money could be factored out
		
	- cons:
		- hard to explain and implement
		- requires strong social cohesion 
		
	[need to discuss how free travel is handled if different communities have different ways of distributing resources] 
	
## Test rendering text from other docs

[doc](./test.md)